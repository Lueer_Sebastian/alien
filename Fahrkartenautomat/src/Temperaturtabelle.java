import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class Temperaturtabelle {

			  public static void main( String[] args )
			  
			  {
			    String[][] Rohdaten = {
			    { "-20", "-28.8889" }, { "-10", "-23.3333" }, { "0", "-17.7778" },
			    { "20", "-6.6667" }, {"30", "-1.11111"}
			    };

			    String[] Überschriften =  {
			      "Fahrenheit", "Celsius"
			    };

			    JFrame f = new JFrame();
			    f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

			    JTable table = new JTable( Rohdaten, Überschriften );
			    f.add( new JScrollPane( table ) );

			    f.pack();
			    f.setVisible( true );
			  }
			}
